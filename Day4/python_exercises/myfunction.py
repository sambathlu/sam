#!/usr/bin/python

from second import MyClass;

def printFunction():
    print("Hello Python!")
    print ( __name__ )


if __name__ == "__main__":
 printFunction()
 obj = MyClass()
 obj.secondFunction()

